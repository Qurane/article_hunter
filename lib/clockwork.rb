# -*- encoding : utf-8 -*-
require 'clockwork'
require './config/boot'
require './config/environment'
require 'delayed_job_active_record'
require 'rake'

module Clockwork

	every(10.minutes, 'hunt_article_parse') { HuntArticleParseJob.perform_later }
	every(20.minutes, 'hunt_article_parse') { HuntHrefParseJob.perform_later }

end
