require 'httparty'
require 'nokogiri'

# этот модуль отвечает за 2 главные вещи: скачивание страниц из интернета и их обработку (получение ссылок, статей и т.д.)
module HuntSearchEngine

	def self.collect_hrefs(hunt_site_id)
		hunt_site = HuntSite.find(hunt_site_id)
		return nil if hunt_site.nil? || !hunt_site.task.is_active

		# получаем ссылку целевого сайта, которую еще не запарсили (если её нет, то парсим сам целевой сайт)
		wanderer = hunt_site.wander_logs.where(href_has_been_parsed: false).first
		target_href = wanderer.nil? ? hunt_site.href : wanderer.href
		return nil if wanderer.nil? && !hunt_site.is_new

		response = self.load_page(target_href, hunt_site.task.id, 0)

		# считаем дочерний линк спаршенным (если он есть, иначе применяем это к сайту), возвращаем nil если код ответа не 200
		hunt_site.update(is_new: false) if wanderer.nil? || hunt_site.is_new
		return nil if response.nil? || response.code != 200
		wanderer.update(href_has_been_parsed: true, href: target_href) unless wanderer.nil?


		# создаем дочерние линки из спаршенной страницы (итерация по всем элементам, ищем атрибут href)
		Nokogiri::HTML(response.body).css('*').each do |node|
			next if node.attribute('href').nil? || !self.url_is_mine(node.attribute('href').value, hunt_site.href)
			href = node.attribute('href').value.start_with?('http') ? 
				node.attribute('href').value : 
				self.combine_full_href(hunt_site.href, node.attribute('href').value)
			HuntWanderLog.create(href: href, hunt_site_id: hunt_site.id)
		end
	end

	def self.collect_article(hunt_wander_log_id)
		hunt_wander_log = HuntWanderLog.find(hunt_wander_log_id)
		return nil if hunt_wander_log.nil? || hunt_wander_log.article_has_been_parsed

		response = self.load_page(hunt_wander_log.href, hunt_wander_log.site.task.id, 1)

		# @TODO: если ответ не 200 (ошибка), то урл все равно считается спаршенным, нужно разделять удачные парсы от не удачных, но не удачные не парсить снова
		# считаем дочерний линк спаршенным (если он есть), возвращаем nil если код ответа не 200
		hunt_wander_log.update(article_has_been_parsed: true)
		return nil if response.nil? || response.code != 200

		nokogiri_doc = Nokogiri::HTML(response.body)

		# парсим заголовки h1, получаем html их родителей
		parents = []
		nokogiri_doc.css('h1').each do |node|
			parents.push({container: node.parent || nil, title: node.text})
		end	

		# парсим тело статьи и его заголовок по классам (не связано с предыдущим блоком)
		info_block_h1s = self.search_in_node(nokogiri_doc, 'class', 'info-block').css('h1')
		nokogiri_doc.css('*').each do |node|
			next if node.attribute('class').nil? || node.attribute('class').value != 'article'
			if node.children.first.name != 'h1' && !info_block_h1s.blank?
				html = info_block_h1s.first.to_html
				title_text = info_block_h1s.first.text
			elsif node.children.first.name == 'h1'
				html = ''
				title_text = node.children.first.text
			else
				html = ''
				title_text = ''
			end
			html += node.to_html
			parents.push({container: Nokogiri::HTML(html).children.children.first.children, title: title_text})
		end

		return nil if parents.count == 0

		# создаем зипись статьи каждого html если длина его текста больше 1000 символов (done создан что бы записи не повторялись)
		done = []
		parents.each do |p|
			next if p[:container].text.length < 1000 || done.include?(p[:container])
			done.push(p[:container])
			HuntArticle.create(title: p[:title], source_href: hunt_wander_log.href, value_html: p[:container].to_html.gsub(/\n/, ''), hunt_site_id: hunt_wander_log.site.id)
		end
	end

	private

		# создаем экземпляр лога данного процесса парсинга, грузим сайт и сохраняем лог
		# href - адрес страницы, которую нужно загрузить (получить ответ)
		# task_id - id задачи, к которой относится сайт
		# target_id - тип лога: href или статья (0, 1 соответственно)
		def self.load_page(href, task_id, target_id)
			hunt_parse_log = HuntParseLog.new(hunt_task_id: task_id, target_href: href, hunt_parse_log_target_id: target_id)
			response = HTTParty.get(href)
			hunt_parse_log.response_status = HuntResponseStatus.where(status_message: response.message, status_code: response.code).first_or_create
			hunt_parse_log.response_html = response.body.encoding.name == "ASCII-8BIT" ? response.body[0..2548].force_encoding("UTF-8") : response.body[0..2548]
			begin
				hunt_parse_log.save!
			rescue => e
				hunt_parse_log.response_html = "exception"
				hunt_parse_log.save!
				return nil
			end
			response
		end

		# метод создан для того, что бы проконтролировать корректность ссылки
		# base = http://hey.ru, second_part = /articles or articles
		def self.combine_full_href(base, second_part)
			second_part = second_part[1..second_part.length-1] if base.last == '/' && second_part.first == '/'
			base += '/' if base.last != '/' && second_part.first != '/'
			return base += second_part
		end

		# поиск в nokogiri node по атрибуту, если значение атрибута не указано, то поиск идет по наличию атрибута, возвращает nokogiri html
		def self.search_in_node(nokogiri_node, attribute_name, attribute_value=nil)
			return [] if nokogiri_node.blank?
			result_array = nokogiri_node.css('*').select{|item| attribute_value.nil? ? !item.attribute(attribute_name).nil? : !item.attribute(attribute_name).nil? && item.attribute(attribute_name).value == attribute_value}
			Nokogiri::HTML(result_array.map{|node| node.to_html}.join(''))
		end

		def self.url_is_mine(target_href, must_starts_with_href)
			begin
				target_uri = URI(target_href)
				must_be_url = URI(must_starts_with_href)
				must_hrefs = [must_starts_with_href, must_be_url.host]
				return target_href.start_with?(*must_hrefs) 
			rescue => e
				return false
			end
		end
end