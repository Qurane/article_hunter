#language: ru
# 
# 
# Перед каждым функционалом есть тег, например @hunt_search_engine
# Перед каждым сценарием есть тег, например @hunt_search_engine_1
# Пример применения: cucumber --tags @hunt_search_engine_1
#
# !!! ВНИМАНИЕ !!!
# Что бы тестить на локальной машине, необходимо запустить тестовый сервер (RAILS_ENV=test rails s) + пока можно тестить ТОЛЬКО на локальной машине
# Если требуется проверить булево значение (через таблицу, например) то следует использовать 1 заместо true
#
#
# 
# =====
# functionality: 1 - hunt_search_engine
# =====
# 

@hunt_search_engine
Функционал: Работоспособность модуля парсинга статей - HuntSearchEngine (и его взаимодействие с базой данных)

# 
# -----
# hunt_search_engine: 1
# -----
# 
@hunt_search_engine_1
Сценарий: Спарсить все ссылки с тестовой страницы (dummy_seeds) id которой 0, стандартные полные ссылки внутри body и head, тестовая страница в hunt site
  Допустим в базе данных есть следующие HuntTask:
    | name              | parse_article_count_per_day | id | is_active |
    | Спарсить статейки | 5                           | 0  | true      |
  И в базе данных есть следующие HuntSite:
    | name              | href                                      | hunt_task_id     | id | is_new  |
    | Статейный сайт    | http://0.0.0.0:3000/hunt/test/page/0      | 0                | 0  | true    |
  Когда hunt search engine парсит ссылки с hunt сайта, id которого 0
  Тогда в базе данных должны быть следующие HuntParseLog:
    | target_href                                 | hunt_task_id | hunt_response_status_id  | hunt_parse_log_target_id |
    | http://0.0.0.0:3000/hunt/test/page/0        | 0            | 0                        | 0                        |
  И в базе данных должны быть HuntParseLog в количестве 1
  И в базе данных должны быть следующие HuntWanderLog:
    | href                                                                          | href_has_been_parsed | hunt_site_id |
    | https://make-3d.ru/blogs/reviews/obzor-flashforge-hunter/                     | false                | 0            |
    | https://make-3d.ru/articles/chto-mozhet-byt-napechatano-na-3d-printere/       | false                | 0            |
    | https://make-3d.ru/                                                           | false                | 0            |
    | https://make-rd.ru/articles                                                   | false                | 0            |
  И в базе данных должны быть HuntWanderLog в количестве 4
  И в базе данных должны быть следующие HuntSite:
    | name              | href                                      | hunt_task_id     | id | is_new   |
    | Статейный сайт    | http://0.0.0.0:3000/hunt/test/page/0      | 0                | 0  | false    |

# 
# -----
# hunt_search_engine: 2
# -----
# 
@hunt_search_engine_2
Сценарий: Спарсить статью с тестовой страницы (dummy_seeds) id которой 0, статья обозначается заголовком h1
  Допустим в базе данных есть следующие HuntTask:
    | name              | parse_article_count_per_day | id | is_active |
    | Спарсить статейки | 5                           | 0  | true      |
  И в базе данных есть следующие HuntSite:
    | name              | href                                      | hunt_task_id     | id |
    | Статейный сайт    | http://0.0.0.0:3000/                      | 0                | 0  |
  И в базе данных есть следующие HuntWanderLog:
    | article_has_been_parsed   | href                                      | hunt_site_id     | id |
    | false                     | http://0.0.0.0:3000/hunt/test/page/0      | 0                | 0  |
  Когда hunt search engine парсит статьи с hunt wander сайта, id которого 0
  Тогда таблица hunt article должна содержать текст статьи из тестовой статьи с id 0
  И таблица hunt article должна содержать html статьи из тестовой статьи с id 0
  И в базе данных должны быть HuntSite в количестве 1
  И в базе данных должны быть следующие HuntWanderLog:
    | href                                        | hunt_site_id | id | article_has_been_parsed |
    | http://0.0.0.0:3000/hunt/test/page/0        | 0            | 0  | 1                       |
  И в базе данных должны быть следующие HuntArticle: 
    | title              | source_href                                    | hunt_article_status_id | hunt_site_id |
    | Супер статья!      | http://0.0.0.0:3000/hunt/test/page/0           | 0                      | 0            |
  И в базе данных должны быть следующие HuntParseLog:
    | target_href                                 | hunt_task_id | hunt_response_status_id  | hunt_parse_log_target_id |
    | http://0.0.0.0:3000/hunt/test/page/0        | 0            | 0                        | 1                        |
  И в базе данных должны быть HuntParseLog в количестве 1

# 
# -----
# hunt_search_engine: 3
# -----
# 
@hunt_search_engine_3
Сценарий: Спарсить все ссылки с тестовой страницы (dummy_seeds) id которой 1, стандартные полные ссылки внутри body и head, обрезанные линки (с слешем в начале и без), тестовая страница в wander log
  Допустим в базе данных есть следующие HuntTask:
    | name              | parse_article_count_per_day | id | is_active |
    | Спарсить статейки | 5                           | 0  | true      |
  И в базе данных есть следующие HuntSite:
    | name              | href                                      | hunt_task_id     | id | is_new   |
    | Статейный сайт    | http://0.0.0.0:3000/                      | 0                | 0  | false    |
  И в базе данных есть следующие HuntWanderLog:
    | href_has_been_parsed   | href                                      | hunt_site_id     | id |
    | false                  | http://0.0.0.0:3000/hunt/test/page/1      | 0                | 0  |  
  Когда hunt search engine парсит ссылки с hunt сайта, id которого 0
  Тогда в базе данных должны быть следующие HuntParseLog:
    | target_href                                 | hunt_task_id | hunt_response_status_id  | hunt_parse_log_target_id |
    | http://0.0.0.0:3000/hunt/test/page/1        | 0            | 0                        | 0                        |
  И в базе данных должны быть HuntParseLog в количестве 1
  И в базе данных должны быть следующие HuntWanderLog:
    | href                                                                          | href_has_been_parsed | hunt_site_id |
    | https://make-3d.ru/blogs/reviews/obzor-flashforge-hunter/                     | false                | 0            |
    | http://0.0.0.0:3000/articles/chto-mozhet-byt-napechatano-na-3d-printere/      | false                | 0            |
    | https://make-3d.ru/                                                           | false                | 0            |
    | http://0.0.0.0:3000/articles                                                  | false                | 0            |
    | http://0.0.0.0:3000/hunt/test/page/1                                          | 1                    | 0            |
  И в базе данных должны быть HuntWanderLog в количестве 5
  И в базе данных должны быть следующие HuntSite:
    | name              | href                                      | hunt_task_id     | id | is_new   |
    | Статейный сайт    | http://0.0.0.0:3000/                      | 0                | 0  | false    |

# 
# -----
# hunt_search_engine: 4
# -----
# 
@hunt_search_engine_4
Сценарий: Спарсить статью с тестовой страницы (dummy_seeds) id которой 0, статья обозначается классом article, заголовок в виде h1 в блоке info-block вне блока статьи
  Допустим в базе данных есть следующие HuntTask:
    | name              | parse_article_count_per_day | id | is_active |
    | Спарсить статейки | 5                           | 0  | true      |
  И в базе данных есть следующие HuntSite:
    | name              | href                                      | hunt_task_id     | id |
    | Статейный сайт    | http://0.0.0.0:3000/                      | 0                | 0  |
  И в базе данных есть следующие HuntWanderLog:
    | article_has_been_parsed   | href                                      | hunt_site_id     | id |
    | false                     | http://0.0.0.0:3000/hunt/test/page/1      | 0                | 0  |
  Когда hunt search engine парсит статьи с hunt wander сайта, id которого 0
  Тогда таблица hunt article должна содержать текст статьи из тестовой статьи с id 1
  И таблица hunt article должна содержать html статьи из тестовой статьи с id 1
  И в базе данных должны быть HuntSite в количестве 1
  И в базе данных должны быть следующие HuntWanderLog:
    | href                                        | hunt_site_id | id | article_has_been_parsed |
    | http://0.0.0.0:3000/hunt/test/page/1        | 0            | 0  | 1                       |
  И в базе данных должны быть следующие HuntArticle: 
    | title                        | source_href                                    | hunt_article_status_id | hunt_site_id |
    | Обзор FLASHFORGE Hunter      | http://0.0.0.0:3000/hunt/test/page/1           | 0                      | 0            |
  И в базе данных должны быть следующие HuntParseLog:
    | target_href                                 | hunt_task_id | hunt_response_status_id  | hunt_parse_log_target_id |
    | http://0.0.0.0:3000/hunt/test/page/1        | 0            | 0                        | 1                        |
  И в базе данных должны быть HuntParseLog в количестве 1









# 
# -----
# hunt_search_engine: 2
# -----
# 
# @hunt_search_engine_2
# Сценарий: Спарсить статью с локальной тестовой страницы page_0 сайта article_site_0
#   Допустим используются локальные данные сайта article_site_0 страницы page_0 
#   И в базе данных есть следующие hunt задачи:
#     | name              | parse_article_count_per_day | 
#     | Спарсить статейки | 5                           |
#   И в базе данных есть следующие HuntArticle:
#     | name              | href                        | hunt_task_id     |
#     | Статейный сайт    | http://article_site_0.ru    | 0                |
#   Когда hunt search engine получает запрос спарсить статьи с локальной страницы page_0 сайта article_site_0
#   Тогда в базе данных должны быть следующие hunt parse логи:
#     | target_href                      | hunt_task_id | hunt_parse_log_status_id | message | status_code |
#     | http://article_site_0.ru/page_0  | 0            | 0                        | OK      | 200         |
#   И в базе данных должны быть следующие HuntWanderLog:
#     | href            | has_been_parsed | hunt_site_id |
#     | awer          | false           | 0            |
#     | bawer         | false           | 0            |
#   И в базе данных должны быть следующие hunt статьи, значения которых берутся локально у сайта article_site_0:
#     | source_href                     | title        | value_html          | value_text         | hunt_article_status_id |
#     | http://article_site_0.ru/page_0 | awer         | page_0_article.html | page_0_article.txt | 0                      |
#   И парсер модуль должен вернуть текст статьи, который находится в page_0_article
#   И парсер модуль должен вернуть html статьи, который находится в page_0_article_html













# Сценарий: Спарсить все ссылки с тестовой страницы page_0
#   Допустим в базе данных есть следующие hunt задачи:
#     | name              | parse_article_count_per_day | 
#     | Спарсить статейки | 5                           |
#   И в базе данных есть следующие hunt сайты:
#     | name              | href                        | hunt_task_id     |
#     | Статейный сайт    | https://make-3d.ru/         | 0                |
#   И в базе данных есть следующие hunt wander логи:
#     | href              | 
#   Когда парсер модуль парсит ссылки с сайта, айди которого 0
#   Тогда в базе данных должны быть логи парсинга, у которых
#     | target_href       | hunt_parse_log_status_id |
#     | 


# Сценарий: Получить 2-ух пользователей с user_id = 0,2, tenant_id = 1 и return = fname,lname,email
#   Допустим в базе данных следующие пользователи:
#     | tenant_id   | user_id  | fname      | lname      | email              |
#     | 0           | 0        | Donald     | Trump      | donald@example.com |
#     | 0           | 1        | Arnold     | Svarzh     | arni@example.com   |
#     | 1           | 0        | Vasya      | Pupkin     | vasya@example.com  |
#     | 1           | 1        | Alesha     | Popovich   | alesha@example.com |
#     | 1           | 2        | John       | Smith      | john@example.com   |
#   Когда модуль получает GET запрос /api/v1/1/users/?user_ids=0,2&return=fname,lname,email&reason=cucumbeer
#   Тогда response-status должен быть 200
#   И response-body должен быть JSON:
#     """
#     [
#       {"id": null, "fname": "Vasya", "lname": "Pupkin", "email": "vasya@example.com"},
#       {"id": null, "fname": "John", "lname": "Smith", "email": "john@example.com"}
#     ]
#     """
#   И в базе данных должны быть 1 экземпляр(а) лога в котором tenant_id: 1, reason: "cucumbeer", status: 200, status_msg: "OK"
