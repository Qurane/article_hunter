Допустим(/^пользователь переходит на страницу (.*)$/) do |url_path|
	begin
		visit(eval(url_path))
	rescue => @visit_error
	end
end

Допустим(/^авторизируется новый пользователь с правами (.*)$/) do |role|
	email = 'test@mail.com'
	password = '123qwe'
	user = User.new(email: email, password: password)

	case role
		when 'пользователя'
			user.role = UserRole.user
		when 'админа'
			user.role = UserRole.admin
		when 'супер админа'
			user.role = UserRole.super_admin
	end
	user.save!

	visit new_user_session_path
	fill_in "user_email", :with => email
	fill_in "user_password", :with => password
	click_button "Log in"  
end

Допустим(/^в базе данных есть следующие ([^$]*):$/) do |data, table|
	(table.raw.count-1).times do |_index|
		record = eval(data).new
		table.raw[_index+1].each_with_index do |element, _index_inner|
			record[table.raw[0][_index_inner]] = table.raw[_index+1][_index_inner]
		end
		record.save
	end
end

Тогда(/^в базе данных должны быть следующие ([^$]*):$/) do |data, table|
	expect_items = []
	(table.raw.count-1).times do |_index|
		expect_item = {}
		table.raw[_index+1].each_with_index do |element, _index_inner|
			expect_item[table.raw[0][_index_inner]] = table.raw[_index+1][_index_inner]
		end
		expect_items.push(expect_item)
	end

	exist_items = []
	expect_items.each do |_expect_item|
		exist_items.push(eval(data).where(_expect_item).first)
	end
	exist_items.compact!

	expect(exist_items.count).to be == expect_items.count
end

Тогда(/^должна быть ошибка запроса: (.*)$/) do |error_message|
	expect(@visit_error.message).to eq(error_message)
end

Тогда(/^пользователь должен окзаться на странице (.*)$/) do |url_path|
	expect(page.current_path).to eq(eval(url_path))
end

И(/^в базе данных должны быть ([^$]*) в количестве (\d+)$/) do |data, count|
	real_count = eval(data).all.count || 0

	expect(real_count).to be == count.to_i
end