require 'hunt_search_engine'

Когда(/^hunt search engine парсит ссылки с hunt сайта, id которого (\d+)$/) do |hunt_site_id|
	HuntSearchEngine.collect_hrefs(hunt_site_id)
end

Когда(/^hunt search engine парсит статьи с hunt wander сайта, id которого (\d+)$/) do |hunt_wander_id|
	HuntSearchEngine.collect_article(hunt_wander_id)
end

Тогда(/^таблица hunt article должна содержать ([^$]*) статьи из тестовой статьи с id (\d+)$/) do |type, hunt_test_page_id|
	case type
		when 'html'
			result = HuntArticle.where("replace(value_html, ' ', '') = '#{HuntTestPage.find(hunt_test_page_id).article_html.gsub(' ', '')}'").first
		when 'текст'
			result = HuntArticle.select{ |a| a.get_article_text.gsub(' ', '') == HuntTestPage.find(hunt_test_page_id).article_text.gsub(' ', '')}.first
	end
	expect(result).to be_truthy
end











# Допустим(/^используется данные тестовой страницы id 0 (\d+)$/) do |test_page_id|
# 	case data_type
# 	when 'локальные'
# 		get("/hunt/test/page/#{test_page_id}")
# 	end
# end

# require 'ext/string'

# Допустим(/^в базе данных следующие пользователи:$/) do |table|
# 	(table.raw.count-1).times do |_index|
# 		u = User.new
# 		table.raw[_index+1].each_with_index do |element, _index_inner|
# 			u[table.raw[0][_index_inner]] = table.raw[_index+1][_index_inner]
# 		end
# 		u.save
# 	end
# end

# Когда(/^модуль получает ([^"]*) запрос ([^"]*)$/) do |type, request|
# 	case type.downcase
# 	when 'get'
# 		get(request)
# 	when 'post'
# 		post(request)
# 	when 'patch'
# 		patch(request)
# 	when 'put'
# 		put(request)
# 	when 'delete'
# 		delete(request)
# 	end
# end

# Когда(/^модуль получает ([^"]*) запрос ([^"]*) с json параметрами (.*)$/) do |type, request, json|
# 	case type.downcase
# 	when 'get'
# 		get(request, params.to_json)
# 	when 'post'
# 		post(request, JSON.parse(ActiveSupport::JSON.decode(json).to_json))
# 	when 'patch'
# 		patch(request, params.to_json)
# 	when 'put'
# 		put(request, params.to_json)
# 	when 'delete'
# 		delete(request, params.to_json)
# 	end
# end

# И(/^response\-status должен быть ([^"]*)$/) do |count|
# 	count = count.to_i

# 	expect(last_response.status).to be == count
# end

# И(/^response\-body должен быть JSON:$/) do |json|
# 	data = JSON.parse(last_response.body)

# 	expect(data.to_json.to_line).to be == json.to_line
# end

# И(/^response\-body должен содержать пользователя:$/) do |table|
# 	expected_item = table.hashes.each_with_object({}) do |row, hash|
# 		name, value = row["attribute"], row["value"]
# 		hash[name] = value
# 	end
# 	data = MultiJson.load(last_response.body)
# 	data.each do |item|
# 		item.each_with_index do |item_inner, _index|
# 			t = expected_item.keys.select {|s| s == item_inner[0]}.first
# 			expected_item.delete(t) if !t.blank? && item_inner[1].to_s == expected_item[t]
# 		end
# 	end
# 	if expected_item.count > 0
# 		data.each_with_index do |item, _index|
# 			t = expected_item.keys.select {|s| s == item[0]}.first
# 			expected_item.delete(t) if !t.blank? && item[1].to_s == expected_item[t]
# 		end
# 	end
# 	expect(expected_item.count).to be == 0
# end

# И(/^response\-body должен иметь (\d+) пользователя\(ей\)$/) do |count|
# 	count = count.to_i
# 	data = JSON.parse(last_response.body)

# 	expect(data).to be_a_kind_of(Array)
# 	expect(data.count).to be == count
# end

# И(/^в базе данных должны быть (\d+) экземпляр\(а\) лога в котором tenant_id: ([^"]*), reason: "([^"]*)", status: ([^"]*), status_msg: "([^"]*)"$/) do |count, tenant_id, reason, status, status_msg|
# 	count = count.to_i
# 	req_arr = Request.where(tenant_id: tenant_id, reason: reason)
# 	req = req_arr.first

# 	expect(req_arr.count).to be == count
# 	if count > 0
# 		expect(req).to be
# 		expect(req.response).to be
# 		expect(req.response[:status]).to be == status.to_i
# 		expect(req.response[:name]).to be == status_msg
# 	end
# end
