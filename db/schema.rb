# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171208220414) do

  create_table "article_statuses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "value", null: false
  end

  create_table "articles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "title", null: false
    t.text "body", null: false
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
    t.bigint "article_status_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "published_at"
    t.index ["article_status_id"], name: "index_articles_on_article_status_id"
  end

  create_table "hunt_article_statuses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "value", null: false
    t.string "description", limit: 512
  end

  create_table "hunt_articles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "title", limit: 512, null: false
    t.string "source_href", limit: 2048, null: false
    t.text "value_html", null: false
    t.bigint "hunt_article_status_id"
    t.bigint "hunt_site_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["hunt_article_status_id"], name: "index_hunt_articles_on_hunt_article_status_id"
    t.index ["hunt_site_id"], name: "index_hunt_articles_on_hunt_site_id"
  end

  create_table "hunt_parse_log_targets", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "value", null: false
  end

  create_table "hunt_parse_logs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "target_href", limit: 2048, null: false
    t.string "response_html", limit: 2550
    t.bigint "hunt_parse_log_target_id"
    t.bigint "hunt_response_status_id"
    t.bigint "hunt_task_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["hunt_parse_log_target_id"], name: "index_hunt_parse_logs_on_hunt_parse_log_target_id"
    t.index ["hunt_response_status_id"], name: "index_hunt_parse_logs_on_hunt_response_status_id"
    t.index ["hunt_task_id"], name: "index_hunt_parse_logs_on_hunt_task_id"
  end

  create_table "hunt_response_statuses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "status_message", limit: 512
    t.integer "status_code"
  end

  create_table "hunt_sites", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name", limit: 512
    t.string "href", limit: 1024, null: false
    t.boolean "is_new", default: true, null: false
    t.bigint "hunt_task_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["hunt_task_id"], name: "index_hunt_sites_on_hunt_task_id"
  end

  create_table "hunt_tasks", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name", null: false
    t.string "description", limit: 1024
    t.integer "parse_article_count_per_day"
    t.boolean "is_active", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "article_parse_at"
    t.datetime "href_parse_at"
    t.integer "parse_href_count_per_day"
    t.integer "article_timer_minutes"
    t.integer "href_timer_minutes"
  end

  create_table "hunt_test_pages", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.text "html", null: false
    t.text "article_html"
    t.text "article_text"
    t.integer "status_code"
  end

  create_table "hunt_wander_logs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "href", limit: 2048, null: false
    t.boolean "href_has_been_parsed", default: false, null: false
    t.boolean "article_has_been_parsed", default: false, null: false
    t.bigint "hunt_site_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["hunt_site_id"], name: "index_hunt_wander_logs_on_hunt_site_id"
  end

  create_table "materialable_tags", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "materialable_type"
    t.bigint "materialable_id"
    t.bigint "tag_id"
    t.index ["materialable_type", "materialable_id"], name: "index_materialable_tags_on_materialable_type_and_materialable_id"
    t.index ["tag_id"], name: "index_materialable_tags_on_tag_id"
  end

  create_table "tags", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_roles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "value", null: false
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.bigint "user_role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["user_role_id"], name: "index_users_on_user_role_id"
  end

end
