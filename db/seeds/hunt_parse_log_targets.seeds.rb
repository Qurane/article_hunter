DatabaseCleaner.clean_with(:truncation,:only => %w[hunt_parse_log_targets])
seed_file = Rails.root.join('db', 'seeds', 'hunt_parse_log_targets.seeds.yml')
data = YAML::load_file(seed_file)
p "HuntParseLogTarget..."
HuntParseLogTarget.create!(data)