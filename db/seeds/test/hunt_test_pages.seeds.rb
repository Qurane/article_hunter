DatabaseCleaner.clean_with(:truncation,:only => %w[hunt_test_pages])
seed_file = Rails.root.join('db', 'seeds', 'test', 'hunt_test_pages.seeds.yml')
data = YAML::load_file(seed_file)
p "HuntTestPage..."
HuntTestPage.create!(data)