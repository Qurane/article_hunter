DatabaseCleaner.clean_with(:truncation,:only => %w[hunt_wander_logs])
seed_file = Rails.root.join('db', 'seeds', 'dummy', 'hunt_wander_logs.seeds.yml')
data = YAML::load_file(seed_file)
p "HuntWanderLog..."
HuntWanderLog.create!(data)