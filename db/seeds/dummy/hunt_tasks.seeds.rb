DatabaseCleaner.clean_with(:truncation,:only => %w[hunt_tasks])
seed_file = Rails.root.join('db', 'seeds', 'dummy', 'hunt_tasks.seeds.yml')
data = YAML::load_file(seed_file)
p "HuntTask..."
HuntTask.create!(data)