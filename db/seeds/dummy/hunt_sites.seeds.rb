DatabaseCleaner.clean_with(:truncation,:only => %w[hunt_sites])
seed_file = Rails.root.join('db', 'seeds', 'dummy', 'hunt_sites.seeds.yml')
data = YAML::load_file(seed_file)
p "HuntSite..."
HuntSite.create!(data)