DatabaseCleaner.clean_with(:truncation,:only => %w[hunt_articles])
seed_file = Rails.root.join('db', 'seeds', 'dummy', 'hunt_articles.seeds.yml')
data = YAML::load_file(seed_file)
p "HuntArticle..."
HuntArticle.create!(data)