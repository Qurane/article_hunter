DatabaseCleaner.clean_with(:truncation,:only => %w[hunt_parse_logs])
seed_file = Rails.root.join('db', 'seeds', 'dummy', 'hunt_parse_logs.seeds.yml')
data = YAML::load_file(seed_file)
p "HuntParseLog..."
HuntParseLog.create!(data)