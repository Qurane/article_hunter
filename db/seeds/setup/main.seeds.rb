p ""
p "Setup: main"

Rake::Task["db:seed:hunt_article_statuses"].invoke
Rake::Task["db:seed:hunt_response_statuses"].invoke
Rake::Task["db:seed:hunt_parse_log_targets"].invoke

Rake::Task["db:seed:user_roles"].invoke
Rake::Task["db:seed:article_statuses"].invoke