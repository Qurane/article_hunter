seed_file = Rails.root.join('db', 'seeds', 'users.seeds.yml')
data = YAML::load_file(seed_file)
p "Users..."
data.each_with_index do |d, index|
	User.first_or_create(data)
	p "#{((index+1)/data.count)*100}%"
end