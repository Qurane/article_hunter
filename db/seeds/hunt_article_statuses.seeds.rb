DatabaseCleaner.clean_with(:truncation,:only => %w[hunt_article_statuses])
seed_file = Rails.root.join('db', 'seeds', 'hunt_article_statuses.seeds.yml')
data = YAML::load_file(seed_file)
p "HuntArticleStatus..."
HuntArticleStatus.create!(data)