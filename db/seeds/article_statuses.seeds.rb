DatabaseCleaner.clean_with(:truncation,:only => %w[article_statuses])
seed_file = Rails.root.join('db', 'seeds', 'article_statuses.seeds.yml')
data = YAML::load_file(seed_file)
p "ArticleStatus..."
ArticleStatus.create!(data)