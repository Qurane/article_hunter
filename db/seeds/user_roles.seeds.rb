DatabaseCleaner.clean_with(:truncation,:only => %w[user_roles])
seed_file = Rails.root.join('db', 'seeds', 'user_roles.seeds.yml')
data = YAML::load_file(seed_file)
p "UserRoles..."
UserRole.create!(data)