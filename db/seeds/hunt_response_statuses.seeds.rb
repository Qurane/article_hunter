DatabaseCleaner.clean_with(:truncation,:only => %w[hunt_response_statuses])
seed_file = Rails.root.join('db', 'seeds', 'hunt_response_statuses.seeds.yml')
data = YAML::load_file(seed_file)
p "HuntResponseStatus..."
HuntResponseStatus.create!(data)