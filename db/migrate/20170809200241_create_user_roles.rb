class CreateUserRoles < ActiveRecord::Migration[5.1]
  def change
    create_table :user_roles do |t|
    	t.string :value, null: false, unique: true
    end
  end
end
