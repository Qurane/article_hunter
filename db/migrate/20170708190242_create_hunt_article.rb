class CreateHuntArticle < ActiveRecord::Migration[5.1]
  def change
    create_table :hunt_articles do |t|
    	t.string :title, null: false, :limit => 512
    	t.string :source_href, null: false, :limit => 2048
    	t.text :value_html, null: false

        t.references :hunt_article_status, index: true
    	t.references :hunt_site, index: true

    	t.timestamps
    end
  end
end
