class CreateHuntArticleStatus < ActiveRecord::Migration[5.1]
  def change
    create_table :hunt_article_statuses do |t|
    	t.string :value, null: false, unique: true
    	t.string :description, :limit => 512
    end
  end
end
