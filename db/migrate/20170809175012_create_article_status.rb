class CreateArticleStatus < ActiveRecord::Migration[5.1]
  def change
    create_table :article_statuses do |t|
    	t.string :value, null: false, unique: true
    end
  end
end
