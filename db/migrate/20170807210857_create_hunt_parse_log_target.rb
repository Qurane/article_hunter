class CreateHuntParseLogTarget < ActiveRecord::Migration[5.1]
  def change
    create_table :hunt_parse_log_targets do |t|
    	t.string :value, null: false, unique: true
    end
  end
end
