class CreateHuntTask < ActiveRecord::Migration[5.1]
  def change
    create_table :hunt_tasks do |t|
    	t.string :name, null: false
    	t.string :description, :limit => 1024
    	t.integer :parse_article_count_per_day
    	t.integer :parse_period_minutes
    	t.boolean :is_active, default: false

    	t.timestamps
    end
  end
end
