class AddParseDatesToHuntTask < ActiveRecord::Migration[5.1]
	def change
		add_column :hunt_tasks, :article_parse_at, :datetime
		add_column :hunt_tasks, :href_parse_at, :datetime
	end
end
