class CreateHuntResponseStatus < ActiveRecord::Migration[5.1]
  def change
    create_table :hunt_response_statuses do |t|
    	t.string :status_message, limit: 512
    	t.integer :status_code
    end
  end
end
