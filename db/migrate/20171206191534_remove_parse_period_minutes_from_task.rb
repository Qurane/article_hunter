class RemoveParsePeriodMinutesFromTask < ActiveRecord::Migration[5.1]
	def self.up
		remove_column :hunt_tasks, :parse_period_minutes
	end

	def self.down
		add_column :hunt_tasks, :parse_period_minutes, :integer
	end
end
