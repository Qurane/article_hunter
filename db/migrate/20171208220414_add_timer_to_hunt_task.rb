class AddTimerToHuntTask < ActiveRecord::Migration[5.1]
	def change
		add_column :hunt_tasks, :parse_href_count_per_day, :integer
		add_column :hunt_tasks, :article_timer_minutes, :integer
		add_column :hunt_tasks, :href_timer_minutes, :integer
	end
end
