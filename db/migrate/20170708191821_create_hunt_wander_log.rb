class CreateHuntWanderLog < ActiveRecord::Migration[5.1]
  def change
    create_table :hunt_wander_logs do |t|
    	t.string :href, null: false, :limit => 2048
    	t.boolean :href_has_been_parsed, null: false, default: false
    	t.boolean :article_has_been_parsed, null: false, default: false

    	t.references :hunt_site, index: true

    	t.timestamps
    end
  end
end
