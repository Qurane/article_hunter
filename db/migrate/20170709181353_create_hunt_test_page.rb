class CreateHuntTestPage < ActiveRecord::Migration[5.1]
  def change
    create_table :hunt_test_pages do |t|
    	t.text :html, null: false
    	t.text :article_html
    	t.text :article_text
    	t.integer :status_code
    end
  end
end
