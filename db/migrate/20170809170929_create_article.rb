class CreateArticle < ActiveRecord::Migration[5.1]
  def change
    create_table :articles do |t|
    	t.string :title, null: false, unique: true
    	t.text :body, null: false
    	t.attachment :image

    	t.references :article_status, index: true

    	t.timestamps
    end
  end
end
