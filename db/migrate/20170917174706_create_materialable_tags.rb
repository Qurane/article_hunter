class CreateMaterialableTags < ActiveRecord::Migration[5.1]
	def change
		create_table :materialable_tags do |t|
			t.references :materialable, polymorphic: true
			t.references :tag
		end
	end
end
