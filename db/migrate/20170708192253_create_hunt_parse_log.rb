class CreateHuntParseLog < ActiveRecord::Migration[5.1]
  def change
    create_table :hunt_parse_logs do |t|
    	t.string :target_href, null: false, :limit => 2048
    	t.string :response_html, :limit => 2550

    	t.references :hunt_parse_log_target, index: true
        t.references :hunt_response_status
    	t.references :hunt_task, index: true

    	t.timestamps
    end
  end
end
