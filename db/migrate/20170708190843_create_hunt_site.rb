class CreateHuntSite < ActiveRecord::Migration[5.1]
  def change
    create_table :hunt_sites do |t|
    	t.string :name, :limit => 512
    	t.string :href, null: false, :limit => 1024
    	t.boolean :is_new, null: false, default: true

    	t.references :hunt_task, index: true

    	t.timestamps
    end
  end
end
