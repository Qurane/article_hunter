function SearchModel() {
	this.articles = [];
	this.articles_search_result = ko.observable([]);
	this.is_show_search_result = ko.observable(true);
	this.search_value = ko.observable('');

	this.search_value.subscribe(function(){
		window.self.search.articles_search_result(window.self.search.articles.filter(function(article){
			return window.self.search.search_value().trim() != '' && article['value_for_search'].includes(window.self.search.search_value());
		}).slice(0,10));
		window.self.search.is_show_search_result(window.self.search.articles_search_result().length > 0);
	});

	$.getJSON("/articles_for_search", function(data) {
		window.self.search.articles = data;
	});
}
