function ArticlesFilterModel() {

	// то, из чего будет динамичный поиск
	this.tags = [];

	// то, что напрямую используется в view
	this.tag_value = ko.observable("");
	this.tag_search_result = ko.observableArray([]);
	this.search_value = ko.observable("");
	this.selected_tags = ko.observableArray([]);
	this.date_from = ko.observable();
	this.date_to = ko.observable();
	this.is_show_search_tags_result = ko.observable(false);

	this.tag_value.subscribe(function(){
		window.self.articles_filter.tag_search_result(window.self.articles_filter.tags.filter(function(tag){
			return window.self.articles_filter.tag_value().trim() != '' && tag['name'].includes(window.self.articles_filter.tag_value());
		}).slice(0,10));
		window.self.articles_filter.is_show_search_tags_result(window.self.articles_filter.tag_search_result().length > 0);
	});

	this.addTag = function(tag_id){
		if(window.self.articles_filter.selected_tags().find(function(t){ return t['id'] === tag_id }) != undefined){
			return;
		}
		window.self.articles_filter.tag_value("");
		window.self.articles_filter.selected_tags().push(window.self.articles_filter.tags.find(function(t){ return t['id'] === tag_id }));
		window.self.articles_filter.selected_tags.valueHasMutated();
	};

	this.removeTag = function(tag_id){
		var target_index = window.self.articles_filter.selected_tags().indexOf(window.self.articles_filter.selected_tags().find(function(x){ return x['id'] === tag_id }))
		if(target_index === -1) {
			return;
		}

		var result = window.self.articles_filter.selected_tags().slice(0, target_index)
		// result.push(...window.self.articles_filter.selected_tags().slice(target_index + 1, window.self.articles_filter.selected_tags().length))

		window.self.articles_filter.selected_tags(result)
	}

	// получаем теги
	$.getJSON("/tags", function(data){
		window.self.articles_filter.tags = data;
	});
}
