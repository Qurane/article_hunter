$(document).on('turbolinks:load', function () {
	function MainModel() {
		self = this;

		self.articles_filter = null;
		self.search = null;

		if(typeof(ArticlesFilterModel) !== "undefined"){
			self.articles_filter = new ArticlesFilterModel();
		}

		if(typeof(SearchModel) !== "undefined"){
			self.search = new SearchModel();
		}
	}

	ko.applyBindings(new MainModel());
});