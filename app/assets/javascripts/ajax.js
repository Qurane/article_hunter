$(document).on("click", ".ajax-event", function(event) {
	var clickObjectAttributes = event.currentTarget.attributes;
	sendAjax(clickObjectAttributes["ajax-type"].value,
		clickObjectAttributes["ajax-url"].value,
		clickObjectAttributes["ajax-data"],value,
		clickObjectAttributes["ajax-data-type"].value,
		clickObjectAttributes["ajax-success-content"].value);
})

function sendAjax(type, url, data, data_type, success_content) {
	$.ajax({
		type: type,
		url: url,
		data: data,
		dataType: data_type,
		success: function(s_data) {
			if(type != "script" && success_content_div != null)
				$(success_content).html(data);
		}
	});
}