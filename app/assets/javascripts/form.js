function image_input_preview(input) {
  var files = input.files;
  var image = files[0]
  var reader = new FileReader();
  reader.onload = function(file) {
    var img = new Image();
    console.log(file);
    img.src = file.target.result;
    console.log(img.src);
    $('#image_preview').html(img);
  }
  reader.readAsDataURL(image);
};

$(document).on("change", ".submit_form_on_change", function(event){
	event.target.parentElement.submit();
})

$(document).on("click", ".remove_parent", function(event){
	event.target.parentElement.remove();
})

var tag_field = "<div class=\"tag_field\">" +
				"<input type=\"text\" value=\"\" name=\"article[tags_attributes][<tag_field_count>][name]\">" +
				"<a class=\"remove_parent\"> X</a>" +
				"</div>";

$(document).on("click", ".add_tag_field", function(event){
	$('.add_tag_field').before(tag_field.replace("<tag_field_count>", $('.tag_field').length + 1));
})