var articleBootstrapTable = {
	url: '/cms/articles.json'
}

var tagBootstrapTable = {
	url: '/cms/tags.json'
}

var tagShowBootstrapTable = {
	// тут нужно в запросе добавить id тега и .json
	url: '/cms/tags/'
}

function refreshBootstrapTable(query) {
	$('#bootstrap_table').bootstrapTable(query);
}