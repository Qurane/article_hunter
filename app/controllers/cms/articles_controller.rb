class Cms::ArticlesController < Layouts::CmsController

	before_action :get_article, only: [:edit, :update, :destroy, :public, :to_archive, :hide, :show]
	before_action :get_statuses, only: [:edit, :new, :update, :create]

	def index
		@articles = Article.not_deleted
		@article_count = @articles.count
		@article_statuses = ArticleStatus.all

		respond_to do |format|
			format.json
			format.html
		end
	end

	def show

	end

	def new
		@article = Article.new
	end

	def create
		@article = Article.create(article_params)

		remove_missing_tags
		if @article.valid?
			redirect_to action: 'index'
		else
			render 'new'
		end
	end

	def edit

	end

	def update
		remove_missing_tags
		if @article.update(article_params)
			respond_to do |format|
				format.html { redirect_to action: 'index' }
				format.js
			end
		else
			respond_to do |format|
				format.html { render 'edit' }
				format.js
			end
		end
	end

	def destroy
		@article.update(status: ArticleStatus.deleted)
		respond_to do |format|
			format.html { redirect_to action: 'index' }
			format.js { refresh_index }
		end
	end

	private

		def refresh_index
			@articles = Article.not_deleted
			render action: 'index'
		end

		def get_article
			begin
				@article = Article.find(params[:id])
			rescue => error
				redirect_to action: 'index'
				return
			end
		end

		def get_statuses
			@statuses = ArticleStatus.all.collect{|s| [s.to_s, s.id] }
		end

		def article_params
			params.require(:article).permit(:title, :body, :image, :article_status_id, tags_attributes: [:id, :name])
		end

		def remove_missing_tags
			return if params[:article][:tags_attributes].blank?
			tags_ids = []
			params[:article][:tags_attributes].keys.each do |k| tags_ids << params[:article][:tags_attributes][k]['id'] end
			tags_ids.compact!
			@article.materialable_tags.where.not(tag_id: tags_ids).delete_all
		end
end
