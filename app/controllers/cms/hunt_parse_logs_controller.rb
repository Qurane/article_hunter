class Cms::HuntParseLogsController < Layouts::CmsController

	before_action :get_log, only: [:show]
	before_action :get_task, only: [:index]

	def index
		if @task == "all_new"
			@logs = HuntParseLog.all
		else
			@logs = @task.parse_logs
		end
	end

	def show

	end

	private

		def get_log
			@log = HuntParseLog.find_by(id: params[:id])
			redirect_to controller: 'Cms::HuntParseLogsController', action: 'index', task: 'all_new' and return if @log.nil?
		end

		def get_task
			redirect_to cms_path if params[:task].blank?
			if params[:task] == "all_new"
				@task = "all_new"
			else
				@task = HuntTask.find_by(id: params[:task])
			end
			redirect_to cms_path and return if @task.nil?
		end

end
