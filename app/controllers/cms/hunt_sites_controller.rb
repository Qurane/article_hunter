class Cms::HuntSitesController < Layouts::CmsController

	before_action :get_site, only: [:edit, :update]
	before_action :get_task, only: [:index]

	def index
		@sites = @task.sites
	end

	def new
		@site = HuntSite.new
		@site.hunt_task_id = params[:hunt_task_id]
	end

	def create
		@hunt_site = HuntSite.create(site_params)
		if @hunt_site.valid?
			redirect_to action: :edit, id: @hunt_site.id
		else
			render :new
		end
	end

	def edit

	end

	def update
		@site.update(site_params)

		if @site.valid?
			if params[:redirect_url].blank?
				redirect_to action: edit
			else
				redirect_to params[:redirect_url]
			end
		else
			render :edit
		end
	end

	private

		def get_site
			@site = HuntSite.find_by(id: params[:id])
			redirect_to cms_hunt_tasks_path and return if @site.nil?
		end

		def site_params
			params.require(:hunt_site).permit(:name, :href, :hunt_task_id)
		end

		def get_task
			@task = HuntTask.find_by(id: params[:task_id])
			redirect_to cms_path and return if @task.nil?
		end

end
