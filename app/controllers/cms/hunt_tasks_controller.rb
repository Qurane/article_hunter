class Cms::HuntTasksController < Layouts::CmsController

	before_action :get_task, only: [:show, :update, :edit]

	def index
		@tasks = HuntTask.all
	end

	def show

	end

	def new
		@task = HuntTask.new
	end

	def create
		@task = HuntTask.create task_params
		if @task.valid?
			redirect_to action: :index
		else
			render :new
		end
	end

	def edit

	end

	def update
		@task.update(task_params)

		if @task.valid?
			redirect_to action: :index
		else
			render :edit
		end
	end

	private

		def get_task
			@task = HuntTask.find_by(id: params[:id])
			redirect_to action: :index and return if @task.nil?
		end

		def task_params
			params.require(:hunt_task).permit(:name, :description, :parse_article_count_per_day, :is_active, :parse_href_count_per_day)
		end

end
