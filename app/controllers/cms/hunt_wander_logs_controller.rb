class Cms::HuntWanderLogsController < Layouts::CmsController

	before_action :get_site, only: [:index]

	def index
		@wander_logs = @site.wander_logs
	end

	private

		def get_site
			@site = HuntSite.find_by(id: params[:site_id])
			redirect_to cms_path and return if @site.nil?
		end

end
