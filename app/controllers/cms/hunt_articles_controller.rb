class Cms::HuntArticlesController < Layouts::CmsController

	before_action :get_article, only: [:update, :show, :to_rewrite]
	before_action :get_task, only: [:index]

	def index
		if @task == "all_new"
			@articles = HuntArticle._new
		else
			@articles = @task.articles
		end
	end

	def show
		@article.update(status: HuntArticleStatus.viewed) if @article.status.new?
	end

	def update
		@article.update(article_params)

		if @article.valid?
			if params[:redirect_url].blank?
				redirect_to action: show
			else
				redirect_to params[:redirect_url]
			end
		else
			render :show
		end
	end

	private

		def get_article
			@article = HuntArticle.find_by(id: params[:id])
			redirect_to controller: 'Cms::HuntArticlesController', action: 'index', task: 'all_new' and return if @article.nil?
		end

		def article_params
			params.require(:hunt_article).permit(:hunt_article_status_id)
		end

		def get_task
			redirect_to cms_path if params[:task].blank?
			if params[:task] == "all_new"
				@task = "all_new"
			else
				@task = HuntTask.find_by(id: params[:task])
			end
			redirect_to cms_path and return if @task.nil?
		end

end
