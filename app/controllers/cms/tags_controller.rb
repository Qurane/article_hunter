class Cms::TagsController < Layouts::CmsController

	before_action :get_tag, only: [:show]

	def index
		@tags = Tag.all
		@tags_count = @tags.count

		respond_to do |format|
			format.html
			format.json
		end
	end

	def show
		@articles = @tag.articles
		@article_count = @articles.count

		respond_to do |format|
			format.html
			format.json
		end
	end

	private

		def get_tag
			begin
				@tag = Tag.find(params[:id])
			rescue => error
				redirect_to action: 'index'
				return
			end
		end

end