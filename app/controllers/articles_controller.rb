class ArticlesController < Layouts::ApplicationController
	include Rails.application.routes.url_helpers

	before_action :define_menu_item
	before_action :check_page, only: [:index]

	def index
		# @articles = Article.paginate(page: params[:page], per_page: @per_page).includes(:tags)

		query = ""
		unless params["tags"].blank?
			query += "("
			params["tags"].each_with_index do |tag, index|
				if index == 0
					query += "tags.name = '#{tag}'"
				else
					query += "or tags.name = '#{tag}'"
				end
			end
			query += ")"
		end
		unless params["date"].blank?
			unless params["date"]["begin_at"].blank?
				query += " and " unless query.blank?
				query += "published_at >= '#{params["date"]["begin_at"]}'"
			end
			unless params["date"]["end_at"].blank?
				query += " and " unless query.blank?
				query += "published_at <= '#{params["date"]["end_at"]}'"
			end
		end
		unless params["search_text"].blank?
			query += " and " unless query.blank?
			query += "title in (#{params['search_text'].split(" ").to_s.gsub(/[\[\]]/, "")})"
		end

		@articles = Article.published.includes(:tags).where(query).references(:tags).paginate(page: params[:page], per_page: @per_page)

		respond_to do |format|
			format.html
			format.js
		end
	end

	def show
		@article = Article.find(params[:id])
	end

	def articles_for_search
		articles_sql = ActiveRecord::Base.connection.exec_query("
			select articles.id as id, articles.title as title, tags.name as tag from articles
				left outer join materialable_tags on 
					materialable_tags.materialable_id = articles.id and
					materialable_type = 'Article'
				left outer join tags on tags.id = materialable_tags.tag_id
		");
		articles = []
		articles_sql.pluck('id').each do |article_id|
			article_sql = articles_sql.select{ |as| as['id'] == article_id }
			article = {}
			article[:href] = article_path(id: article_id)
			article[:title] = article_sql.first['title']
			article[:value_for_search] = (article[:title] + ' ' + article_sql.pluck('tag').join(' ')).strip
			articles.push(article)
		end
		respond_to do |format|
			format.json { render json: articles.to_json }
		end
	end

	private

		def define_menu_item
			set_active_menu_item(@menu_items[:articles])
		end

		def check_page
			@per_page = 5
			params[:page] = 1 if params[:page].nil?
			redirect_to action: 'index', page: 1 and return if params[:page].to_i < 1 || Article.all.count < @per_page * (params[:page].to_i - 1)
		end
end
