class TagsController < Layouts::ApplicationController

	def index
		@tags = Tag.all.select(:id, :name)

		respond_to do |format|
			format.json { render json: @tags.to_json }
		end
	end

end