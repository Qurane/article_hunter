class Layouts::CmsController < ActionController::Base
	protect_from_forgery with: :exception
	layout 'cms'

	before_action :check_user

	def index
		redirect_to cms_articles_path
	end

	def check_user
		redirect_to root_path and return if current_user.role.is_user?
	end
end
