class Layouts::ApplicationController < ActionController::Base
	protect_from_forgery with: :exception
	layout 'application'

	before_action :get_tags
	before_action :define_menu

	def get_tags
		@tags = Tag.all.take(10)
	end

	def define_menu
		@menu_items = {:main => 'main', :articles => 'articles'}
		@active_menu_item = {}
	end

	def set_active_menu_item item
		@active_menu_item = item
	end
end
