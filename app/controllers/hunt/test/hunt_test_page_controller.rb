class Hunt::Test::HuntTestPageController < Layouts::ApplicationController
	layout false

	def show
		page = HuntTestPage.find(params[:id])
		@html = page.html
		response.status = page.status_code
	end
end
