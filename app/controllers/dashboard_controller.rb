class DashboardController < Layouts::ApplicationController

	before_action :define_menu_item

	def show
		@articles = Article.paginate(page: params[:page], per_page: 5).includes(:tags)
	end

	private

		def define_menu_item
			set_active_menu_item(@menu_items[:main])
		end
end