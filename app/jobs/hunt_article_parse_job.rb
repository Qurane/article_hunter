require 'hunt_search_engine'

class HuntArticleParseJob < ActiveJob::Base
	queue_as :default

	def perform(*args)
		Delayed::Worker.logger.info "HuntArticleParseJob start"
		HuntTask.can_parse_article.each do |task|
			task.parse_article
			task.update(article_parse_at: DateTime.now + task.article_timer_minutes.minutes)
		end
		Delayed::Worker.logger.info "HuntArticleParseJob end"
	end
end
