require 'hunt_search_engine'

class HuntHrefParseJob < ActiveJob::Base
	queue_as :default

	def perform(*args)
		Delayed::Worker.logger.info "HuntHrefParseJob start"
		HuntTask.can_parse_href.each do |task|
			task.parse_href
			task.update(href_parse_at: DateTime.now + task.href_timer_minutes.minutes)
		end
		Delayed::Worker.logger.info "HuntHrefParseJob end"
	end
end
