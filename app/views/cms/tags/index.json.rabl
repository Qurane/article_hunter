object false
node(:total) { @tags_count }
child(@tags, root: :rows, object_root: false) do
	attributes :id
	node :name do |tag|
		link_to tag.name, cms_tag_path(tag)
	end
	node :article_count do |tag|
		tag.articles.count
	end
end
node(:rows, if: @tags.empty? ) { [] }