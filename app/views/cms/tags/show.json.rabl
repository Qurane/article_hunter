object false
node(:total) { @article_count }
child(@articles, root: :rows, object_root: false) do
	attributes :id
	node :title do |article|
		link_to article.title, edit_cms_article_path(article)
	end
	node :article_statuse do |article|
		article.status.to_s
	end
end
node(:rows, if: @articles.empty?) { [] }