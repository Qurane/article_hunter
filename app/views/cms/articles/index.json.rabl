object false
node(:total) { @article_count }
child(@articles, root: :rows, object_root: false) do
	attributes :id
	node :title do |article|
		link_to article.title, edit_cms_article_path(article)
	end
	node :article_status_id do |article|
		form_tag(cms_article_path(article), method: :put, remote: true, authenticity_token: true, id: "article_#{article.id}") do
			select_tag("article[article_status_id]", options_from_collection_for_select(@article_statuses, "id", :to_s, article.article_status_id), class: "submit_form_on_change")
		end
	end
end
node(:rows, if: @articles.empty? ) { [] }
