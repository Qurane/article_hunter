class User < ApplicationRecord
	# Include default devise modules. Others available are:
	# :confirmable, :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable,
		   :rememberable, :trackable, :validatable

	belongs_to :role, class_name: 'UserRole', foreign_key: 'user_role_id'

	before_validation :set_role, on: :create

	private

		def set_role
			self.role = UserRole.user if self.role.nil?
		end
end
