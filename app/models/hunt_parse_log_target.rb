class HuntParseLogTarget < ApplicationRecord
	has_many :parse_logs, class_name: 'HuntParseLog', foreign_key: 'hunt_parse_log_target_id'

end