require 'nokogiri'

class HuntArticle < ApplicationRecord
	belongs_to :site, class_name: 'HuntSite', foreign_key: 'hunt_site_id'
	belongs_to :status, class_name: 'HuntArticleStatus', foreign_key: 'hunt_article_status_id'	

	scope :_new, -> { where(status: HuntArticleStatus._new) }

	before_validation :set_status, on: :create

	def get_article_html
		self.value_html
	end

	def get_article_text
		Nokogiri::HTML(self.value_html).text
	end

	private
	
		def set_status
			self.status = HuntArticleStatus.where(value: 'New').first if self.status.nil?
		end
end