class Tag < ApplicationRecord
	has_many :materialable_tags

	has_many :articles, through: :materialable_tags, source: :materialable, source_type: 'Article'
end