class UserRole < ApplicationRecord
  	has_many :users

  	def is_user?
  		self.value == 'User'
  	end

  	def is_admin?
  		self.value == 'Admin'
  	end

  	def is_super_admin?
  		self.value == 'Super admin'
  	end

  	def self.user
  		UserRole.find_by value: 'User'
  	end

  	def self.admin
  		UserRole.find_by value: 'Admin'
  	end

  	def self.super_admin
  		UserRole.find_by value: 'Super admin'
  	end
end
