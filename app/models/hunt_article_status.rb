class HuntArticleStatus < ApplicationRecord
	has_many :articles, class_name: 'HuntArticle', foreign_key: 'hunt_article_status_id'

	scope :_new, -> { find_by(value: "New") }
	scope :viewed, -> { find_by(value: "Viewed") }
	scope :rewritten, -> { find_by(value: "Rewritten") }
	scope :useless, -> { find_by(value: "Useless") }
	scope :not_article, -> { find_by(value: "Not article") }
	scope :to_rewrite, -> { find_by(value: "To rewrite") }

	scope :available_for_edit, -> { where.not(value: "New").where.not(value: "Viewed") }

	def new?
		self.value == "New"
	end

	def viewed?
		self.value == "Viewed"
	end

	def rewritten?
		self.value == "Rewritten"
	end

	def useless?
		self.value == "Useless"
	end

	def not_article?
		self.value == "Not article"
	end

	def to_rewrite?
		self.value == "To rewrite"
	end

end