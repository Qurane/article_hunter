require 'hunt_search_engine'

class HuntTask < ApplicationRecord

	has_many :sites, class_name: 'HuntSite', foreign_key: 'hunt_task_id'
	has_many :wander_logs, class_name: 'HuntWanderLog', foreign_key: 'hunt_site_id', through: :sites
	has_many :parse_logs, class_name: 'HuntParseLog', foreign_key: 'hunt_task_id'	
	has_many :articles, class_name: 'HuntArticle', foreign_key: 'hunt_site_id', through: :sites

	validates_presence_of :parse_article_count_per_day
	validates_presence_of :parse_href_count_per_day

	scope :can_parse_article, -> { where("article_parse_at <= ?", DateTime.now) }
	scope :can_parse_href, -> { where("href_parse_at <= ?", DateTime.now) }

	before_create :set_parse_datetimes
	before_create :generate_timers
	before_update :generate_timers

	def parse_article
		# self.sites.can_parse_article.each do |site|
			target_wander_log = self.sites.can_parse_article.last.wander_logs.can_parse_article.first
			return if target_wander_log.nil?
			HuntSearchEngine.collect_article(target_wander_log.id)
		# end
	end

	def parse_href
		# self.sites.can_parse_href.each do |site|
			HuntSearchEngine.collect_hrefs(self.sites.can_parse_href.last.id)		
		# end
	end

	private

		def set_parse_datetimes
			self.article_parse_at = DateTime.now if self.article_parse_at.nil?
			self.href_parse_at = DateTime.now if self.href_parse_at.nil?
		end

		def generate_timers
			self.article_timer_minutes = 1440 / self.parse_article_count_per_day
			self.href_timer_minutes = 1440 / self.parse_href_count_per_day
		end

end