class HuntWanderLog < ApplicationRecord
	belongs_to :site, class_name: 'HuntSite', foreign_key: 'hunt_site_id'

	scope :can_parse_article, -> { where(article_has_been_parsed: false) }
	scope :can_parse_href, -> { where(href_has_been_parsed: false) }

end