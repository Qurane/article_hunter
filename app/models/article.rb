class Article < ApplicationRecord
	belongs_to :status, class_name: 'ArticleStatus', foreign_key: 'article_status_id'

	has_attached_file :image, path: :image_path, url: :image_url, default_url: :default_image_url
	validates_attachment_content_type :image, content_type: ['image/jpeg', 'image/png'],
		message: 'Изображение должно иметь формат jpeg или png'

	has_many :materialable_tags, as: :materialable
	has_many :tags, through: :materialable_tags

	accepts_nested_attributes_for 	:tags

	validates :body, presence: { message: 'Текст статьи должен иметь значение.' }
	validates :title, presence: { message: 'Заголовок статьи должен иметь значение.' }, uniqueness: { message: 'Заголовок статьи должен быть уникальным.' }

	before_validation :set_status, on: :create

	scope :deleted, -> { where status: ArticleStatus.deleted }
	scope :not_deleted, -> { where.not status: ArticleStatus.deleted }
	scope :published, -> { where status: ArticleStatus.published }

	private

		def set_status
			self.status = ArticleStatus.not_public if self.status.nil?
		end

		def image_path
			"#{Rails.root}/public/articles/#{self[:id]}/#{self[:title]}.#{self[:image_content_type].split('/').last}"
		end

		def image_url
			"/articles/#{self[:id]}/#{self[:title]}.#{self[:image_content_type].split('/').last}"
		end

		def default_image_url
			"/images/article.png"
		end
end