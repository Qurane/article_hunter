class HuntParseLog < ApplicationRecord
	belongs_to :task, class_name: 'HuntTask', foreign_key: 'hunt_task_id'
	belongs_to :response_status, class_name: 'HuntResponseStatus', foreign_key: 'hunt_response_status_id'
	belongs_to :target, class_name: 'HuntParseLogTarget', foreign_key: 'hunt_parse_log_target_id'

	def type
		case self.hunt_parse_log_target_id
		when 0
			return "Ссылка"
		when 1
			return "Статья"
		end
	end

end