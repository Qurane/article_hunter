class ArticleStatus < ApplicationRecord
	has_many :articles

	validates :value, presence: { message: 'Значение статуса статьи должно существовать.' }, uniqueness: { message: 'Значение статуса статьи должно быть уникально.' }

	def to_s
		if is_not_public?
			result = 'Не опубликована'
		elsif is_published?
			result = 'Опубликована'
		elsif is_archived?
			result = 'В архиве'
		elsif is_deleted?
			result = 'Удалена'
		else 
			result = ''
		end
		result
	end

	def is_not_public?
		self.value == 'Not public'
	end

	def is_published?
		self.value == 'Published'
	end

	def is_archived?
		self.value == 'Archived'
	end

	def is_deleted?
		self.value == 'Deleted'
	end

	def self.not_public
		ArticleStatus.find_by value: 'Not public'
	end

	def self.published
		ArticleStatus.find_by value: 'Published'
	end

	def self.archived
		ArticleStatus.find_by value: 'Archived'
	end

	def self.deleted
		ArticleStatus.find_by value: 'Deleted'
	end
end