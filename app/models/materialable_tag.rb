class MaterialableTag < ApplicationRecord
	belongs_to :materialable, polymorphic: true
	belongs_to :tag
end