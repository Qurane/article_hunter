class HuntSite < ApplicationRecord
	
	belongs_to :task, class_name: 'HuntTask', foreign_key: 'hunt_task_id'
	has_many :wander_logs, class_name: 'HuntWanderLog', foreign_key: 'hunt_site_id'
	has_many :articles, class_name: 'HuntArticle', foreign_key: 'hunt_site_id'

	scope :can_parse_article, -> { joins(:wander_logs).where("hunt_wander_logs.article_has_been_parsed = ?", false) }
	scope :can_parse_href, -> { joins("left outer join hunt_wander_logs on hunt_wander_logs.hunt_site_id = hunt_sites.id").where("hunt_wander_logs.href_has_been_parsed = ? or is_new = ?", false, true) }

end