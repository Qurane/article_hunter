class HuntResponseStatus < ApplicationRecord
	has_many :parse_logs, class_name: 'HuntParseLog', foreign_key: 'hunt_response_status_id'
end