Rails.application.routes.draw do
	devise_for :users, path: '', path_names: { sign_in: 'sign_in', sign_out: 'sign_out'}
	# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

	namespace "hunt" do
		namespace "test" do
			resources :page, controller: :hunt_test_page, only: [:show]
		end
	end

	authenticated :user do
		get 'cms/', to: 'layouts/cms#index'

		scope 'cms', as: 'cms', module: 'cms' do
			resources :articles, only: [:index, :new, :create, :edit, :update, :destroy, :show]
			
			resources :hunt_articles, only: [:show, :update]
			get 'hunt_articles/:task/index', to: 'hunt_articles#index', as: 'hunt_articles'

			resources :hunt_tasks, only: [:index, :show, :new, :create, :edit, :update]
			
			resources :hunt_sites, only: [:index, :new, :create, :edit, :update]

			resources :tags, only: [:index, :show]

			get 'hunt_wander_logs/:site_id', to: 'hunt_wander_logs#index', as: 'hunt_wander_logs'

			resources :hunt_parse_logs, only: [:show]
			get 'hunt_parse_logs/:task/index', to: 'hunt_parse_logs#index', as: 'hunt_parse_logs'
		end
	end

	root 'dashboard#show'
	resources :articles, only: [:index, :show]
	post 'articles', to: 'articles#index', as: 'articles_data'
	get 'articles_for_search', to: 'articles#articles_for_search'
	resources :tags, only: [:index]
end
